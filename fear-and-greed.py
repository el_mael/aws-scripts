import requests
import json

url = "https://api.alternative.me/fng"

response = requests.get(url).text
data = json.loads(response)

index = float(data['data'][0]['value'])
index = round(index/10)
classification = data['data'][0]['value_classification']

match index:
    case 1:
        index_str =  "■ □ □ □ □ □ □ □ □ □" 
    case 2:
      index_str =  "□ ■ □ □ □ □ □ □ □ □" 
    case 3:
      index_str =  "□ □ ■ □ □ □ □ □ □ □" 
    case 4:
      index_str =  "□ □ □ ■ □ □ □ □ □ □" 
    case 5:
      index_str =  "□ □ □ □ ■ □ □ □ □ □" 
    case 6:
      index_str =  "□ □ □ □ □ ■ □ □ □ □" 
    case 7:
      index_str =  "□ □ □ □ □ □ ■ □ □ □" 
    case 8:
      index_str =  "□ □ □ □ □ □ □ ■ □ □" 
    case 9:
      index_str =  "□ □ □ □ □ □ □ □ ■ □" 
    case 10:
      index_str =  "□ □ □ □ □ □ □ □ □ ■" 

requests.post("https://ntfy.sh/ergo-bear-whale", data=index_str.encode('utf-8'), 
headers={
    "Title": classification.upper(),
    "Tags": "alert"
})
