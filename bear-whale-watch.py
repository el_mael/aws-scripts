import json
import requests

url = "https://api.ergoplatform.com/api/v1/addresses/9hyDXH72HoNTiG2pvxFQwxAhWBU8CrbvwtJDtnYoa4jfpaSk1d3/balance/confirmed"
ntfy_string = []

response = requests.get(url).text
data = json.loads(response)
ergs = data['nanoErgs'] / (10 ** 9)
ntfy_string.append(f"ERG: {ergs:,.4f}")


# if there are tokens
try:
    for token in data['tokens']:
        ntfy_string.append(f"{token['name']: token['amount']:,.2f}")
except:
    pass
ntfy_qq = str()
for each in ntfy_string:
    ntfy_qq = ntfy_qq + f"{each}\n"

requests.post("https://ntfy.sh/ergo-bear-whale", data=ntfy_qq.encode('utf-8'), 
headers={
    "Title": "ERGO Bear Whale Balance",
    "Tags": "alert"
})

