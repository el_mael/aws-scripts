import requests
from datetime import datetime as dt
import json

# weather api data
api_key = 'd8f2d1de0b6e4ad8a44110524232311'
base_url = 'http://api.weatherapi.com/v1/forecast.json?'
request_url = base_url + f'key={api_key}&' + f'q=Cebu&days=1'

response = requests.get(request_url)
str_data = response.text
data = json.loads(str_data)

# location
region = data['location']['region']
country = data['location']['country']

# weather condition
max_temp = data['forecast']['forecastday'][0]['day']['maxtemp_c']
max_wind = data['forecast']['forecastday'][0]['day']['maxwind_kph']
condition = data['forecast']['forecastday'][0]['day']['condition']['text']

# reporting
#print('Location:', region, country)
#print('Condition:', condition)
#print('Max Temp:', max_temp,'\nMax Wind:', max_wind)
ntfy_str = f"Location: {region}, {country}\nCondition: {condition}\nMax Temp: {max_temp}\nMax Wind: {max_wind}"
print(ntfy_str)

# post to ntfy
requests.post("https://ntfy.sh/12103041-weather", data=ntfy_str.encode('utf-8'), 
headers={
    "Title": "Weather Alerts",
})
